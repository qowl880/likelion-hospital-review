package com.springboot.security;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LikelionHospitalreviewGitlabApplication {

    public static void main(String[] args) {
        SpringApplication.run(LikelionHospitalreviewGitlabApplication.class, args);
    }

}
